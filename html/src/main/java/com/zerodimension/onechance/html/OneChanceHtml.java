package com.zerodimension.onechance.html;

import com.zerodimension.onechance.core.OneChance;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;

public class OneChanceHtml extends GwtApplication {
	@Override
	public ApplicationListener getApplicationListener () {
		return new OneChance();
	}
	
	@Override
	public GwtApplicationConfiguration getConfig () {
		return new GwtApplicationConfiguration(640, 480);
	}
}
