package com.zerodimension.onechance.core.model.traps;

import com.badlogic.gdx.math.Vector2;
import com.zerodimension.onechance.core.model.Player;

/**
 * @author romankov.
 */
public class PitTrap extends RectangularTrap {

    public PitTrap() {
        super(new Vector2(40, 8));
    }

    @Override
    public boolean checkHit(Player player) {
        return inside(player);
    }

    @Override
    public float getDistance(Player player) {
        return getPosition().dst(player.getPosition());
    }

    @Override
    public PitTrap clone() {
        return new PitTrap();
    }
}
