package com.zerodimension.onechance.core.renderers.traps;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.zerodimension.onechance.core.model.World;
import com.zerodimension.onechance.core.model.traps.SensoredPressTrap;

/**
 * @author romankov.
 */
public class SensoredPressTrapRenderer implements TrapRenderer<SensoredPressTrap> {

    private final Animation press;
    private SpriteBatch spriteBatch;

    public SensoredPressTrapRenderer(SpriteBatch spriteBatch, TextureAtlas textureAtlas) {
        this.spriteBatch = spriteBatch;
        press = new Animation(0.2f, textureAtlas.findRegions("trap_sensored"), Animation.LOOP);
    }

    @Override
    public void render(SensoredPressTrap trap, World world) {
        Vector2 trapPosition = trap.getPosition();
        float trapWidth = trap.getRect().getWidth();
        float trapStartPositionX = trapPosition.x - world.getMinX();
        spriteBatch.begin();
        spriteBatch.draw(press.getKeyFrame(1), trapStartPositionX, trap.getRect().y);
        spriteBatch.end();
    }
}
