package com.zerodimension.onechance.core.renderers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.zerodimension.onechance.core.model.World;

/**
 * Created by Indie Developer
 * User: toha
 * Date: 12/14/13
 */
public class ProbabilityRenderer {
    private final BitmapFont font;
    private final float width = 40;
    public ProbabilityRenderer() {
        font = new BitmapFont(Gdx.files.internal("data/fonts/sagas-14.fnt"));

    }

    public void render(ShapeRenderer shapeRenderer, SpriteBatch spriteBatch, World world) {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        Vector2 playerPosition = world.getPlayer().getPosition().cpy().sub(world.getMinX(), 0);
        float y = playerPosition.y + world.getPlayer().getSize().y + 10;
        float x = playerPosition.x - world.getPlayer().getSize().x / 2;
        shapeRenderer.rect(x - width / 2, y, width, 20);
        shapeRenderer.end();

        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append((int) (world.getTrapChance() * 100)).append("%");
        String str = stringBuffer.toString();
        final BitmapFont.TextBounds bounds = font.getBounds(str);
        spriteBatch.begin();
        font.draw(spriteBatch, str, x - bounds.width / 2, y + 5 + bounds.height);
        spriteBatch.end();
    }
}
