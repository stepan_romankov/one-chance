package com.zerodimension.onechance.core.model.traps;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.zerodimension.onechance.core.model.Player;

/**
 * @author romankov.
 */
public abstract class Trap implements Cloneable {
    private final Vector2 position = new Vector2();
    private boolean passed = false;

    public abstract boolean checkHit(final Player player);

    public final boolean checkPass(final Player player) {
        if (player.isAlive() && !passed && checkPassInternal(player)) {
            passed = true;
        }
        return passed;
    }

    protected abstract boolean checkPassInternal(final Player player);

    public abstract Trap clone();

    public abstract Vector2 getPosition();

    public abstract Rectangle getRect();

    public boolean isPassed() {
        return passed;
    }

    public abstract void placeAt(float x, float y);

    public abstract float getDistance(Player player);

    public void updateTime(float dt, Player player) {

    }
}
