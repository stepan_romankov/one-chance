package com.zerodimension.onechance.core.model.traps;

import com.badlogic.gdx.math.Vector2;
import com.zerodimension.onechance.core.model.Player;

/**
 * @author romankov.
 */
public class SensoredPressTrap extends RectangularTrap {
    private final float velocityDown = -200;
    private final float velocityUp = 100;
    private float idleY = 200;
    private State state = State.IDLE;
    private float floorY;

    public SensoredPressTrap() {
        super(new Vector2(250, 50));
    }

    @Override
    public boolean checkHit(Player player) {
        return inside(player);
    }

    @Override
    public void updateTime(float dt, Player player) {
        if (isPassed()) {
            return;
        }
        if (state == State.IDLE) {
            if (player.getPosition().x >= getRect().x &&
                    player.getPosition().x <= getRect().x + getRect().getWidth()) {
                state = State.MOVE_DOWN;
            }
        }

        if (state == State.MOVE_DOWN) {
            float y = getRect().y + velocityDown * dt;
            if (y <= floorY) {
                y = floorY;
                state = State.MOVE_UP;
            }
            getRect().y = y;
        } else if (state == State.MOVE_UP) {
            float y = getRect().y + velocityUp * dt;
            if (y >= floorY + idleY) {
                y = floorY + idleY;
                state = State.IDLE;
            }
            getRect().y = y;
        }
    }

    @Override
    public void placeAt(float x, float y) {
        floorY = y;
        super.placeAt(x, y + idleY);
    }

    @Override
    public SensoredPressTrap clone() {
        return new SensoredPressTrap();
    }

    private enum State {
        IDLE,
        MOVE_UP,
        MOVE_DOWN
    }
}
