package com.zerodimension.onechance.core.renderers.traps;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.zerodimension.onechance.core.model.World;
import com.zerodimension.onechance.core.model.traps.PitTrap;

/**
 * Created by Indie Developer
 * User: toha
 * Date: 12/14/13
 */
public class PitTrapRenderer implements TrapRenderer<PitTrap> {
    private final SpriteBatch spriteBatch;
    private final Animation pit;

    public PitTrapRenderer(SpriteBatch spriteBatch, TextureAtlas textureAtlas) {
        this.spriteBatch = spriteBatch;
        pit = new Animation(0.2f, textureAtlas.findRegions("trap_pit"), Animation.LOOP);
    }


    @Override
    public void render(PitTrap trap, World world) {
        Vector2 trapPosition = trap.getPosition();
        float trapWidth = trap.getRect().getWidth();
        float height = trap.getRect().getHeight();
        float trapStartPositionX = trapPosition.x - world.getMinX();

        spriteBatch.begin();
        spriteBatch.draw(pit.getKeyFrame(1), trapStartPositionX, trap.getRect().y);
        spriteBatch.end();

    }
}
