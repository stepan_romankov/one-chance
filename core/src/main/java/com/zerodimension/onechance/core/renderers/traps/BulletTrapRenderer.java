package com.zerodimension.onechance.core.renderers.traps;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.zerodimension.onechance.core.model.World;
import com.zerodimension.onechance.core.model.traps.BulletTrap;

/**
 * Created by Indie Developer
 * User: toha
 * Date: 12/14/13
 */
public class BulletTrapRenderer implements TrapRenderer<BulletTrap> {
    private final Animation bullet;
    private final SpriteBatch spriteBatch;
    private float time;

    public BulletTrapRenderer(SpriteBatch spriteBatch, final TextureAtlas textureAtlas) {
        this.spriteBatch = spriteBatch;
        bullet = new Animation(0.2f, textureAtlas.findRegions("bullet"), Animation.LOOP);
    }

    @Override
    public void render(BulletTrap trap, World world) {
        Vector2 trapPosition = trap.getPosition();
        float trapWidth = trap.getRect().getWidth();
        float height = trap.getRect().getWidth();
        float trapStartPositionX = trapPosition.x - world.getMinX();
        spriteBatch.begin();
        spriteBatch.draw(bullet.getKeyFrame(time), trapStartPositionX, trapPosition.y);
        spriteBatch.end();
        time += Gdx.graphics.getDeltaTime();
//        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
//        shapeRenderer.triangle(trapStartPositionX, trapPosition.y,
//                trapStartPositionX + trapWidth, trapPosition.y + height / 2,
//                trapStartPositionX + trapWidth, trapPosition.y - height / 2);
//        shapeRenderer.end();
    }
}
