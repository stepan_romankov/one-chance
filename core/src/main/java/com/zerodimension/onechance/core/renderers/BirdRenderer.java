package com.zerodimension.onechance.core.renderers;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.zerodimension.onechance.core.model.World;

/**
 * Created by Indie Developer
 * User: toha
 * Date: 12/15/13
 */
public class BirdRenderer {
    private final Animation animation;

    public BirdRenderer(TextureAtlas textureAtlas) {
        animation = new Animation(0.2f, textureAtlas.findRegions("bat"), Animation.LOOP);
    }

    public void render(SpriteBatch spriteBatch, World world) {
        spriteBatch.begin();
        spriteBatch.draw(animation.getKeyFrame(world.getTime()), world.getBirdPosition().x - world.getMinX(), world.getBirdPosition().y);
        spriteBatch.end();
    }
}
