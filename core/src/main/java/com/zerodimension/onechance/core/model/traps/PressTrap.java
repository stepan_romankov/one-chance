package com.zerodimension.onechance.core.model.traps;

import com.badlogic.gdx.math.Vector2;
import com.zerodimension.onechance.core.model.Player;

/**
 * @author romankov.
 */
public class PressTrap extends RectangularTrap {
    private final Vector2 playerHeadPosition = new Vector2();
    private float velocity = 100;
    private int period = 2;
    private float time;
    private float initialY;


    public PressTrap() {
        super(new Vector2(56, 200));
    }

    @Override
    public boolean checkHit(Player player) {
        boolean hit = inside(player);
        if (hit) {
            player.ricochet();
        }
        return inside(player);
    }

    @Override
    public PressTrap clone() {
        return new PressTrap();
    }

    @Override
    public void updateTime(float dt, Player player) {
        time += dt;
        int k = ((int)(time * 1000f)) % (period * 2 * 1000);
        if (k > period * 1000) {
            k = 2 * period * 1000 - k;
        }
        float y = initialY + velocity * (k / 1000f);
        super.placeAt(getPosition().x, y);
    }

    @Override
    public void placeAt(float x, float y) {
        time = 0;
        initialY = y;
        super.placeAt(x, y);

    }
}
