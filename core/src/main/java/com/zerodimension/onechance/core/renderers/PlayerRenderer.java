package com.zerodimension.onechance.core.renderers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.Vector2;
import com.zerodimension.onechance.core.model.Player;
import com.zerodimension.onechance.core.model.World;

/**
 * Created by Indie Developer
 * User: toha
 * Date: 12/14/13
 */
public class PlayerRenderer {
    private final Animation runAnimation;
    private final Animation standAnimation;
    private final Animation sitAnimation;
    private final Animation deadAnimation;
    private float time;
    private final Sprite sprite;
    private boolean flip;
    private float deathTime;


    public PlayerRenderer(final TextureAtlas textureAtlas) {
        runAnimation = new Animation(0.2f, textureAtlas.findRegions("man"), Animation.LOOP_PINGPONG);
        standAnimation = new Animation(0.2f, textureAtlas.findRegions("man_stand"));
        sitAnimation = new Animation(0.2f, textureAtlas.findRegions("man_sit"));
        deadAnimation = new Animation(1f, textureAtlas.findRegions("man_death"));
        sprite = new Sprite();
        deathTime = 0;
    }

    public void render(SpriteBatch spriteBatch, World world) {
        time += Gdx.graphics.getDeltaTime();
        Vector2 geometry = world.getPlayer().getSize();
        Vector2 playerPosition = world.getPlayer().getPosition();
//        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
//        if (world.getPlayer().isAlive()) {
//            shapeRenderer.setColor(Color.WHITE);
//        } else {
//            shapeRenderer.setColor(Color.RED);
//        }
//        shapeRenderer.rect(playerPosition.x - world.getMinX() - geometry.x, playerPosition.y, geometry.x, geometry.y);
//        shapeRenderer.end();
        spriteBatch.begin();
        TextureRegion textureRegion;
        if (world.getPlayer().getState() == Player.State.SITTING) {
            textureRegion = sitAnimation.getKeyFrame(0.2f);
        } else if (world.getPlayer().getState() == Player.State.DEAD) {
            textureRegion = deadAnimation.getKeyFrame(deathTime);
            deathTime += Gdx.graphics.getDeltaTime();
        } else if (world.getPlayer().getRunDirection() == Player.RunDirection.NONE) {
            textureRegion = standAnimation.getKeyFrame(0.2f);
        } else {
            textureRegion = runAnimation.getKeyFrame(time);
            flip = world.getPlayer().getRunDirection() == Player.RunDirection.LEFT;
        }
        sprite.setRegion(textureRegion);
        sprite.flip(flip, false);
        sprite.setBounds(playerPosition.x - world.getMinX() - (sprite.getWidth() + geometry.x) / 2, playerPosition.y, textureRegion.getRegionWidth(), textureRegion.getRegionHeight());

        sprite.draw(spriteBatch);
        spriteBatch.end();
    }
}
