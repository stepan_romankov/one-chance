package com.zerodimension.onechance.core.model;

import com.badlogic.gdx.math.Vector2;

import java.util.EnumMap;
import java.util.Map;

/**
 * @author romankov.
 */
public class Player {
    private static final float MAX_RUN_SPEED = 300;
    private static final float RUN_ACCELERATION = 500;

    private static final float JUMP_SPEED = 300;
    private static final float GRAVITY_ACCELERATION = -500;

    private static final Map<State, Vector2> SIZE_BY_STATE = new EnumMap<State, Vector2>(State.class){{
        put(Player.State.STANDING, new Vector2(16, 64));
        put(Player.State.JUMPING, new Vector2(16, 64));
        put(Player.State.SITTING, new Vector2(32, 40));
        put(Player.State.DEAD, new Vector2(64, 24));
    }};

    private final World world;
    private Vector2 velocity = new Vector2();
    private Vector2 acceleration = new Vector2(0, 0);
    private RunDirection runDirection = RunDirection.NONE;
    private Vector2 position = new Vector2(200, 200);
    private State state = State.STANDING;
    private boolean alive = true;
    private DeathListener deathListener;

    public Player(final World world) {
        this.world = world;
        this.position.x = world.getMinX() + getSize().x * 5;
        this.position.y = world.getFloorY(position.x);
    }

    public void updateTime(final float dt) {

        acceleration.x = 0;
        if (position.y == world.getFloorY(position.x)) {
            acceleration.y = 0;
            float multiplier = 1;
            if (runDirection == RunDirection.NONE || state == State.SITTING) {
                if (Math.abs(velocity.x) > 10) {
                    multiplier *= - Math.signum(velocity.x);
                    if (state == State.SITTING) {
                        multiplier *= 0.5f;
                    }
                } else {
                    velocity.x = 0;
                    multiplier = 0;
                }

            } else {
                multiplier *= runDirection.getMultiplier();
            }

            acceleration.x = RUN_ACCELERATION * multiplier;

        } else {
            acceleration.x = 0;
            acceleration.y = GRAVITY_ACCELERATION;
        }
        velocity.x += acceleration.x * dt;
        velocity.x = Math.min(velocity.x, MAX_RUN_SPEED);
        velocity.y += acceleration.y * dt;

        velocity.x = (float)Math.floor(velocity.x);
        velocity.y = (float)Math.floor(velocity.y);

        position.x += velocity.x * dt;
        position.y += velocity.y * dt;

        if (position.x < world.getMinX()) {
            position.x =  world.getMinX();
            velocity.x = 0;
        }
        if (world.getPreviousTrap() != null && velocity.x < 0) {
            float trapRigth = world.getPreviousTrap().getPosition().x + world.getPreviousTrap().getRect().getWidth();
            if (getPosition().x - getSize().x < trapRigth) {
                getPosition().x =  trapRigth + getSize().x;
            }

        }
        position.y = Math.max(position.y, world.getFloorY(position.x));
    }

    public void run(final RunDirection runDirection) {
        if (state == State.DEAD) {
            return;
        }
        this.runDirection = runDirection;
    }

    public void jump() {
        if (state == State.DEAD) {
            return;
        }
        if (position.y > world.getFloorY(position.x)) {
            return;
        }
        velocity.y = JUMP_SPEED;
    }

    public void die() {
        if (alive) {
            alive = false;
            state = State.DEAD;
            runDirection = RunDirection.NONE;
            deathListener.onDeath();
        }
    }

    public Vector2 getPosition() {
        return position;
    }

    public State getState() {
        return state;
    }

    public RunDirection getRunDirection() {
        return runDirection;
    }

    public boolean isAlive() {
        return alive;
    }

    public void sitDown() {
        if (state == State.DEAD) {
            return;
        }
        state = State.SITTING;
    }

    public void standUp() {
        if (state == State.DEAD) {
            return;
        }
        state = State.STANDING;
    }

    public static Vector2 getSize(State state) {
        return SIZE_BY_STATE.get(state);
    }

    public Vector2 getSize() {
        return SIZE_BY_STATE.get(this.state);
    }

    public void ricochet() {
        velocity.x = -velocity.x;
    }

    public void ricochet(float v) {
        velocity.x += v;
    }

    public void setDeathListener(DeathListener deathListener) {
        this.deathListener = deathListener;
    }

    public enum RunDirection {
        NONE(0),
        RIGHT(1),
        LEFT(-1);

        private final float multiplier;

        RunDirection(final float multiplier) {

            this.multiplier = multiplier;
        }

        public float getMultiplier() {
            return multiplier;
        }
    }

    public enum State {
        STANDING,
        SITTING,
        DEAD, JUMPING
    }
}
