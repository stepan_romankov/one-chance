package com.zerodimension.onechance.core.renderers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.zerodimension.onechance.core.model.World;

/**
 * Created by Indie Developer
 * User: toha
 * Date: 12/15/13
 */
public class HudRenderer {

    private final BitmapFont font;
    private final BitmapFont cfont;

    public HudRenderer() {
        font = new BitmapFont(Gdx.files.internal("data/fonts/sagas.fnt"));
        cfont = new BitmapFont(Gdx.files.internal("data/fonts/sagas_10.fnt"));
    }

    public void render(SpriteBatch spriteBatch, World world) {
        spriteBatch.begin();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Run distance: ").append((int)(world.getPlayer().getPosition().x / 10)).append(" m");
        String meterString = stringBuffer.toString();
        font.draw(spriteBatch, meterString, 10, Gdx.graphics.getHeight() - font.getBounds(meterString).height + 10);
        if (!world.getPlayer().isAlive())
        {
            String title1 = "You lost your only chance to survive...\n" +
                    "Press SPACE to get one more chance.";

            font.drawMultiLine(spriteBatch, title1, 0, 300, Gdx.graphics.getWidth(), BitmapFont.HAlignment.CENTER);

            String copy1 =
                    "(c) Zero Dimension 2013  \n" +
                    "(c) 'Black Vortex' sound track by Kevin MacLeod incompetech.com  \n" +
                    "(c) 'Pixel Musketeer' font  by Pixel Sagas pixelsagas.com  ";

            cfont.drawMultiLine(spriteBatch, copy1, 0, 40, Gdx.graphics.getWidth(), BitmapFont.HAlignment.RIGHT);


        }
        spriteBatch.end();
    }
}
