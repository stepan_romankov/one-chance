package com.zerodimension.onechance.core.renderers.traps;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.zerodimension.onechance.core.model.World;
import com.zerodimension.onechance.core.model.traps.TunnelTrap;

/**
 * Created by Indie Developer
 * User: toha
 * Date: 12/14/13
 */
public class TunnelTrapRenderer implements TrapRenderer<TunnelTrap> {
    private final SpriteBatch spriteBatch;
    private final Animation tunnel;
    private float time;

    public TunnelTrapRenderer(SpriteBatch spriteBatch, TextureAtlas textureAtlas) {
        this.spriteBatch = spriteBatch;
        tunnel = new Animation(0.2f, textureAtlas.findRegions("trap_tunnel"), Animation.LOOP);
    }

    @Override
    public void render(TunnelTrap trap, World world) {
        time += Gdx.graphics.getDeltaTime();
        Vector2 trapPosition = trap.getPosition();
        float trapWidth = trap.getRect().getWidth();
        float trapStartPositionX = trapPosition.x - world.getMinX();

        spriteBatch.begin();
        spriteBatch.draw(tunnel.getKeyFrame(time), trapStartPositionX, trap.getRect().y);
        spriteBatch.end();
    }
}
