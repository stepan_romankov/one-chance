package com.zerodimension.onechance.core.model;

/**
 * @author romankov.
 */
public interface DeathListener {
    void onDeath();
}
