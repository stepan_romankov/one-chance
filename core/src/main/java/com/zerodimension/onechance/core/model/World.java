package com.zerodimension.onechance.core.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.zerodimension.onechance.core.model.traps.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author romankov.
 */
public class World {

    public static final int INITIAL_FLOOR_Y = 80;
    private final Player player;
    private List<Trap> traps;
    private List<Vector2> torches = new ArrayList<Vector2>();
    private Vector2 birdPosition = new Vector2(-50, 0);
    private float birdSpawningInterval = 10;
    private float prevBirdSpawningTime = 0;
    private final Random random = new Random();
    private final float dx;
    private float x;
    private final Trap[] trapTemplates;
    private float time = 0;

    public World(final float dx) {
        this.dx = dx;
        this.x = 0;
        this.traps = new ArrayList<Trap>();
        player = new Player(this);

        this.trapTemplates = new Trap[]{
                new PitTrap(),
                new TunnelTrap(),
                new PressTrap(),
                new BulletTrap(),
                new SensoredPressTrap()
        };

        torches.add(new Vector2());
        torches.add(new Vector2());
        torches.add(new Vector2( -100, -100));
        torches.add(new Vector2( -100, -100));
        createTrap();
    }



    public Player getPlayer() {
        return player;
    }

    public float getFloorY(final float x) {
        final float fallIntervalAcceleration = 0.99f;
        final float fallInterval = 0.5f * (float)Math.pow(fallIntervalAcceleration, time);
        int n = ((int) x) / 8;
        float timeout = n * fallInterval ;
        float y = INITIAL_FLOOR_Y; //initial Y§
        final float acceleration = 200;
        if (timeout < time && x < getCurrentTrap().getPosition().x && y > 0) {
            y -= acceleration * (time - timeout) * (time - timeout) / 2;
        }
        return y;
    }

    public float getMinX() {
        return x;
    }

    public float getMaxX() {
        return x + dx;
    }

    public void updateTime(float dt) {
        for(Trap trap : traps) {
            trap.updateTime(dt, player);
        }

        if (getFloorY(player.getPosition().x) >= INITIAL_FLOOR_Y) {
            player.updateTime(dt);
        } else {
            player.getPosition().y = getFloorY(player.getPosition().x);
            if (player.getPosition().y < 0) {
                 player.die();
            }
        }

        x = Math.max(x, player.getPosition().x - dx);
        if (getCurrentTrap().checkHit(player)) {
            player.die();
        } else if (getCurrentTrap().checkPass(player)) {
            createTrap();
        }
        updateBirdPosition();
        time += dt;
    }

    public Trap getCurrentTrap() {
        return traps.get(traps.size() - 1);
    }

    public Trap getPreviousTrap() {
        if (traps.size() > 1) {
            return traps.get(traps.size() - 2);
        } else {
            return null;
        }

    }

    private void createTrap() {
        Trap trap = trapTemplates[random.nextInt(trapTemplates.length)].clone();
        final float trapX = x + dx * (2 + random.nextFloat() * 2);
        trap.placeAt(trapX, getFloorY(x));
        traps.add(trap);
        updateTorchPositions();
    }

    private void updateTorchPositions() {
        torches.get(0).set(torches.get(2));
        torches.get(1).set(torches.get(3));
        final Vector2 begin;
        if (getPreviousTrap() == null) {
            begin = new Vector2(0, 0);
        } else {
            begin = getPreviousTrap().getPosition();
        }

        torches.get(2).set((begin.x * 2 + getCurrentTrap().getPosition().x) / 3, getFloorY(x) + 100);
        torches.get(3).set((begin.x + getCurrentTrap().getPosition().x * 2) / 3, getFloorY(x) + 100);
        if (torches.get(2).x < getMaxX()) {
            torches.get(2).x = -100;
        }
        if (torches.get(3).x < getMaxX()) {
            torches.get(3).x = -100;
        }
    }

    private void updateBirdPosition() {
        if (time - prevBirdSpawningTime > birdSpawningInterval) {
            birdPosition.set(getMinX() + Gdx.graphics.getWidth(), 300);
            prevBirdSpawningTime = getTime();
        } else if (birdPosition.x > getMinX() - 50) {
            birdPosition.sub(3, 0).set(birdPosition.x, 200 + (float) (50 * (Math.sin(2 * getTime()) + 1)));
        }
    }

    public float getTrapChance() {
        float distance = getCurrentTrap().getDistance(player);
        if (distance > dx) {
            return 1;
        } else {
            return distance / (dx);
        }
    }


    public float getTime() {
        return time;
    }

    public Vector2 getBirdPosition() {
        return birdPosition;
    }

    public float getDx() {
        return dx;
    }

    public List<Vector2> getTorches() {
        return torches;
    }

    public List<Trap> getTraps() {
        return traps;
    }
}
