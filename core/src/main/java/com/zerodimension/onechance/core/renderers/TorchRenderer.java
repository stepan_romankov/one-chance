package com.zerodimension.onechance.core.renderers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.zerodimension.onechance.core.model.World;

/**
 * @author romankov.
 */
public class TorchRenderer {
    private float time;
    private final Animation animation;

    public TorchRenderer(TextureAtlas textureAtlas) {
        animation = new Animation(0.2f, textureAtlas.findRegions("torch"), Animation.LOOP);
    }

    public void render(SpriteBatch spriteBatch, World world) {
        time += Gdx.graphics.getDeltaTime();
        spriteBatch.begin();
        float i = 0.2f;
        for(Vector2 pos : world.getTorches()) {
            spriteBatch.draw(animation.getKeyFrame(time + i), pos.x - world.getMinX(), pos.y);
            i += 0.2f;
        }
        spriteBatch.end();
    }
}
