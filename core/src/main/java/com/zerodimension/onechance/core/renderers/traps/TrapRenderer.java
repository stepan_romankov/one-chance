package com.zerodimension.onechance.core.renderers.traps;

import com.zerodimension.onechance.core.model.World;
import com.zerodimension.onechance.core.model.traps.Trap;

/**
 * Created by Indie Developer
 * User: toha
 * Date: 12/14/13
 */
public interface TrapRenderer<T extends Trap> {
    void render(T trap, World world);
}
