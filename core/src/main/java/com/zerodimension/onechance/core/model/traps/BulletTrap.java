package com.zerodimension.onechance.core.model.traps;

import com.badlogic.gdx.math.Vector2;
import com.zerodimension.onechance.core.model.Player;

/**
 * @author romankov.
 */
public class BulletTrap extends RectangularTrap {
    private final float velocity = -200;

    public BulletTrap() {
        super(new Vector2(10, 5));
    }

    @Override
    public boolean checkHit(Player player) {
        boolean hit = inside(player);
        if (hit) {
            player.ricochet(velocity);
        }
        return inside(player);
    }

    @Override
    public Trap clone() {
        return new BulletTrap();
    }

    @Override
    public float getDistance(Player player) {
        return super.getDistance(player) * 2.0f;
    }

    @Override
    public void updateTime(float dt, Player player) {

        super.placeAt(getPosition().x + velocity * dt, getPosition().y);
    }

    @Override
    public void placeAt(float x, float y) {
        super.placeAt(x, y + Player.getSize(Player.State.SITTING).y - 5);
    }
}
