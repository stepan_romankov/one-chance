package com.zerodimension.onechance.core.model.traps;

import com.badlogic.gdx.math.Vector2;
import com.zerodimension.onechance.core.model.Player;

/**
 * @author romankov.
 */
public class TunnelTrap extends RectangularTrap {
    private final Vector2 playerHeadPosition = new Vector2();
    public TunnelTrap() {
        super(new Vector2(56, 120));
    }

    @Override
    public boolean checkHit(Player player) {
        boolean hit = inside(player);
        if (hit) {
            player.ricochet();
        }
        return inside(player);
    }

    @Override
    public void placeAt(float x, float y) {
        float trapY = y + (Player.getSize(Player.State.STANDING).y * 2 + Player.getSize(Player.State.SITTING).y) / 3;
        super.placeAt(x, trapY);
    }

    @Override
    public TunnelTrap clone() {
        return new TunnelTrap();
    }
}
