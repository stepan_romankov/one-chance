package com.zerodimension.onechance.core;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.zerodimension.onechance.core.model.DeathListener;
import com.zerodimension.onechance.core.model.Player;
import com.zerodimension.onechance.core.model.World;
import com.zerodimension.onechance.core.model.traps.*;
import com.zerodimension.onechance.core.renderers.*;
import com.zerodimension.onechance.core.renderers.traps.*;

import java.util.HashMap;
import java.util.Map;

public class OneChance extends InputAdapter implements ApplicationListener {
    private ShapeRenderer shapeRenderer;
    private SpriteBatch spriteBatch;
    private PlayerRenderer playerRenderer;
    private ProbabilityRenderer probabilityRenderer;
    private WorldRenderer worldRenderer;
    private ShadowRenderer shadowRenderer;
    private TorchRenderer torchRenderer;
    private BirdRenderer birdRenderer;
    private HudRenderer hudRenderer;
    private TextureAtlas gameAtlas;
    private Map<Class<?>, TrapRenderer> trapRenderers;
    private boolean started = false;

    private World world;
    private Music music;
    private DeathListener deathListener;

    @Override
	public void create () {
        music = Gdx.audio.newMusic(Gdx.files.internal("data/music/music.mp3"));
        music.setLooping(true);
        music.setVolume(0.2f);
        music.play();
        gameAtlas = new TextureAtlas("data/textures/atlas/game.atlas");
        shapeRenderer = new ShapeRenderer();
        spriteBatch = new SpriteBatch();
        playerRenderer = new PlayerRenderer(gameAtlas);
        trapRenderers = new HashMap<Class<?>, TrapRenderer>();
        initTrapRenderers();

        deathListener = new DeathSoundPlyaer();
        restart();
        
        worldRenderer = new WorldRenderer(gameAtlas);
        probabilityRenderer = new ProbabilityRenderer();
        shadowRenderer = new ShadowRenderer(gameAtlas);
        torchRenderer = new TorchRenderer(gameAtlas);
        birdRenderer = new BirdRenderer(gameAtlas);
        hudRenderer = new HudRenderer();
        Gdx.input.setInputProcessor(this);
	}

    private void initTrapRenderers() {
        trapRenderers.put(PitTrap.class, new PitTrapRenderer(spriteBatch, gameAtlas));
        trapRenderers.put(TunnelTrap.class, new TunnelTrapRenderer(spriteBatch, gameAtlas));
        trapRenderers.put(PressTrap.class, new PressTrapRenderer(spriteBatch, gameAtlas));
        trapRenderers.put(BulletTrap.class, new BulletTrapRenderer(spriteBatch, gameAtlas));
        trapRenderers.put(SensoredPressTrap.class, new SensoredPressTrapRenderer(spriteBatch, gameAtlas));

    }

    @Override
	public void resize (int width, int height) {
	}

	@Override
	public void render () {
        float dt = Gdx.graphics.getDeltaTime();
        if (started) {
            world.updateTime(dt);
        }

		Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        worldRenderer.render(spriteBatch, world);
        for (int i = world.getTraps().size() - 1; i >= 0; i--) {
            Trap trap = world.getTraps().get(i);
            if (trap.getPosition().x > world.getMinX() - trap.getRect().getWidth()) {
                TrapRenderer currentTrapRenderer = trapRenderers.get(trap.getClass());
                currentTrapRenderer.render(trap, world);
            } else {
                break;
            }
        }
        shapeRenderer.setColor(Color.WHITE);
        torchRenderer.render(spriteBatch, world);

        if (!started) {
            spriteBatch.begin();
            spriteBatch.draw(gameAtlas.findRegion("main"), 30, 0);
            spriteBatch.end();
        }
        birdRenderer.render(spriteBatch, world);
        if (world.getPlayer().isAlive() && started) {
            shadowRenderer.render(spriteBatch, world);
        }

        playerRenderer.render(spriteBatch, world);
        probabilityRenderer.render(shapeRenderer, spriteBatch, world);
        hudRenderer.render(spriteBatch, world);
	}

	@Override
	public void pause () {
	}

	@Override
	public void resume () {
	}

	@Override
	public void dispose () {
	}

    @Override
    public boolean keyDown(int keycode) {
        return handlePressKey(keycode, true);
    }

    @Override
    public boolean keyUp(int keycode) {
        return handlePressKey(keycode, false);
    }

    private boolean handlePressKey(int keycode, boolean pressed) {
        if (!started) {
            started = keycode == Input.Keys.SPACE && pressed;
            return true;
        }
        boolean processed = false;
            switch(keycode) {
                case Input.Keys.RIGHT: {
                    if (pressed) {
                        world.getPlayer().run(Player.RunDirection.RIGHT);
                    } else if (world.getPlayer().getRunDirection() == Player.RunDirection.RIGHT) {
                        world.getPlayer().run(Player.RunDirection.NONE);
                    }
                    processed = true;
                    break;
                }
                case Input.Keys.LEFT: {
                    if (pressed) {
                        world.getPlayer().run(Player.RunDirection.LEFT);
                    } else if (world.getPlayer().getRunDirection() == Player.RunDirection.LEFT) {
                        world.getPlayer().run(Player.RunDirection.NONE);
                    }
                    processed = true;
                    break;
                }
                case Input.Keys.UP: {
                    if (pressed) {
                        world.getPlayer().jump();
                    }
                    processed = true;
                    break;
                }
                case Input.Keys.DOWN: {
                    if (pressed) {
                        world.getPlayer().sitDown();
                    } else if (world.getPlayer().getState() == Player.State.SITTING) {
                        world.getPlayer().standUp();
                    }
                    processed = true;
                    break;
                }
                case Input.Keys.SPACE:
                case Input.Keys.ENTER: {
                    if (pressed && !world.getPlayer().isAlive()) {
                        restart();
                    }
                    processed = true;
                    break;
                }
        }

        return processed;
    }

    private void restart() {
        world = new World(Gdx.graphics.getWidth() * 0.8f);
        world.getPlayer().setDeathListener(this.deathListener);
    }


    public static class DeathSoundPlyaer implements DeathListener {
        private final Sound sound = Gdx.audio.newSound(Gdx.files.internal("data/sound/death.wav"));

        @Override
        public void onDeath() {
            sound.play(0.3f);
        }
    }
}
