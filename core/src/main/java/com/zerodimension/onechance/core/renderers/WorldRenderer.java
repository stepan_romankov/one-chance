package com.zerodimension.onechance.core.renderers;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.zerodimension.onechance.core.model.World;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Indie Developer
 * User: toha
 * Date: 12/14/13
 */
public class WorldRenderer {

    private final float STEP;
    private final int PIXEL_WIDTH = 8;
    private final TextureAtlas textureAtlas;
    private final Sprite groundSprite;
    private final int textureRegionX;

    public WorldRenderer(final TextureAtlas textureAtlas) {
        this.textureAtlas = textureAtlas;
        STEP = textureAtlas.findRegion("ground").getRegionWidth();
        groundSprite = new Sprite();
        groundSprite.setRegion(textureAtlas.findRegion("ground"));
        groundSprite.setSize(PIXEL_WIDTH, textureAtlas.findRegion("ground").getRegionHeight());
        textureRegionX = groundSprite.getRegionX();
    }

    public void render(SpriteBatch spriteBatch, World world) {
        List<Vector2> ticksPositions = new ArrayList<Vector2>();
        int countTick = (int) ((int) (world.getMaxX() - world.getMinX()) / STEP) + 2;
        int startTickIndex = (int) (world.getMinX() / STEP);
        for (int i = 0; i < countTick; i++) {
            float tickPositionX = (startTickIndex + i) * STEP - world.getMinX();
            float tickPositionY = world.getFloorY(tickPositionX);
            ticksPositions.add(new Vector2(tickPositionX, tickPositionY));
        }
        spriteBatch.begin();
        for (Vector2 tickPosition : ticksPositions) {
            TextureRegion ground = textureAtlas.findRegion("ground");
            for(int i = 0; i < ground.getRegionWidth() / PIXEL_WIDTH; i++) {
                int regionPosition = i * PIXEL_WIDTH;
                groundSprite.setRegionX(textureRegionX + regionPosition);
                groundSprite.setRegionWidth(PIXEL_WIDTH);
                float positionFloorX = tickPosition.x + regionPosition;
                groundSprite.setPosition(positionFloorX, world.getFloorY(positionFloorX + world.getMinX()) - ground.getRegionHeight());
                groundSprite.draw(spriteBatch);
            }
            spriteBatch.draw(textureAtlas.findRegion("wall"), tickPosition.x, World.INITIAL_FLOOR_Y);
        }
        spriteBatch.end();
    }
}
