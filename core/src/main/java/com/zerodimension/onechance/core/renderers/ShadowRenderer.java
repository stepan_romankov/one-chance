package com.zerodimension.onechance.core.renderers;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.zerodimension.onechance.core.model.World;

/**
 * Created by Indie Developer
 * User: toha
 * Date: 12/15/13
 */
public class ShadowRenderer {

    private final TextureAtlas textureAtlas;

    public ShadowRenderer(TextureAtlas textureAtlas) {
        this.textureAtlas = textureAtlas;
    }

    public void render(SpriteBatch spriteBatch, World world) {
        spriteBatch.begin();
        spriteBatch.draw(textureAtlas.findRegion("shadow"), world.getDx() - 5, -20);
        spriteBatch.end();
    }
}
