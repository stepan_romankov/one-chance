package com.zerodimension.onechance.core.model.traps;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.zerodimension.onechance.core.model.Player;

/**
 * @author romankov.
 */
public abstract class RectangularTrap extends Trap {
    private Rectangle rect = new Rectangle();
    private Vector2 position = new Vector2();

    protected RectangularTrap(Vector2 size) {
        this.rect.setSize(size.x, size.y);
    }

    protected boolean inside(Player player) {
        Rectangle playerRect = getPlayerRect(player);
        return  rect.overlaps(playerRect);
    }

    @Override
    public float getDistance(Player player) {
        Vector2[] playerPoints = new Vector2[]{
                new Vector2(player.getPosition().x, player.getPosition().y),
                new Vector2(player.getPosition().x, player.getPosition().y + player.getSize().y),
                new Vector2(player.getPosition().x - player.getSize().x, player.getPosition().y),
                new Vector2(player.getPosition().x - player.getSize().x, player.getPosition().y + player.getSize().y),
        };
        float dst = Float.MAX_VALUE;
        for(Vector2 point : playerPoints) {
            dst = Math.min(dst, Intersector.distanceSegmentPoint(
                    rect.getX(), rect.getY(), rect.getX(), rect.getY() + rect.getHeight(),
                    point.x, point.y
            ));
        }
       return dst;
    }

    @Override
    protected boolean checkPassInternal(Player player) {
        return rect.x + rect.getWidth() < player.getPosition().x - player.getSize().x;
    }

    private Rectangle getPlayerRect(Player player) {
        Rectangle playerRect = new Rectangle();
        playerRect.setPosition(player.getPosition().x - player.getSize().x, player.getPosition().y);
        playerRect.setSize(player.getSize().x, player.getSize().y);
        return playerRect;
    }



    @Override
    public void placeAt(float x, float y) {
        rect.setPosition(x, y);
    }

    public Rectangle getRect() {
        return rect;
    }

    @Override
    public Vector2 getPosition() {
        return rect.getPosition(position);
    }
}
